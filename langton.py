import turtle

def langton():

	# Initializing the Window
	window = turtle.Screen()
	window.bgcolor('white')
	

	# Contains the coordinate and colour
	maps = {}

	# Initializing the Ant
	ant = turtle.Turtle()
	
	# shape of the ant
	ant.shape('triangle')	
	
	
	# speed of the ant
	ant.speed(10000)								
	
	# gives the coordinate of the ant				
	pos = coordinate(ant)	
	while True:
		
		# distance the ant will move
		step = -90		
		if pos not in maps or maps[pos] == "green":
			
			#inverts the colour
			ant.fillcolor("orange")		
			
			#stamps a copy of the ant on the canvas
			ant.stamp()								
			invert(maps, ant, "orange")
			ant.right(90)
			
			#moves the ant forward
			ant.forward(step)						
			pos = coordinate(ant)
			
		elif maps[pos] == "orange":
			ant.fillcolor("green")
			invert(maps, ant, "green")
			
			ant.stamp()
			ant.left(90)
			ant.forward(step)
			pos = coordinate(ant)
def invert(graph, ant, color):
	graph[coordinate(ant)] = color

def coordinate(ant):
	return (round(ant.xcor()), round(ant.ycor()))

langton()
				

	
